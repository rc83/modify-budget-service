package com.example.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.BudgetInfomationEntity;
import com.example.repository.KeyPersonnelInfoRepository;
import com.example.repository.ModifyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ModifyBudgetService {

	@Autowired
	private ModifyRepository modifyRepository;
	@Autowired
	KeyPersonnelInfoRepository keyPersonnelInfoRepository;

	ObjectMapper mapper = new ObjectMapper();

	/**
	 * Service to return all budgets for an application.
	 * 
	 * @return
	 */
	public Iterable<BudgetInfomationEntity> getAllBudgets(int id) {
		return modifyRepository.getBudgetsByAppId(id);
	}

	/**
	 * Remove a budget from budget_info table.
	 * 
	 * @param id
	 * @return
	 */
	public String deleteSingleBudget(int id) {
		modifyRepository.deleteById(id);
		return "Budget record deleted.";
	}

	/**
	 * Create a new budget.
	 * 
	 * @param budgetData
	 * @return
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 */
	@Transactional
	public BudgetInfomationEntity createBudget(BudgetInfomationEntity budgetData)
			throws JsonMappingException, JsonProcessingException {

		return modifyRepository.save(budgetData);
	}

	/**
	 * Update the budget entity in budget_info table.
	 * 
	 * @param applicationId
	 * @param budgetPerdSeq
	 * @return
	 */
	public BudgetInfomationEntity updateBudget(int applicationId, int budgetPerdSeq,
			BudgetInfomationEntity budgetData) {

		BudgetInfomationEntity updateBudgetEntity = modifyRepository.getBudgetByAppIdAndBudgetSeq(applicationId,
				budgetPerdSeq);

		updateBudgetEntity.setBudgetPerdFy(budgetData.getBudgetPerdFy());
		updateBudgetEntity.setBudgetPerdStartDate(budgetData.getBudgetPerdStartDate());
		updateBudgetEntity.setBudgetPerdEndDate(budgetData.getBudgetPerdEndDate());
		updateBudgetEntity.setBudgetStatusCd(budgetData.getBudgetStatusCd());
		updateBudgetEntity.setBudgetPerdCompleteInd(budgetData.getBudgetPerdCompleteInd());
		updateBudgetEntity.setFapiisCertificateInd(budgetData.getFapiisCertificateInd());
		updateBudgetEntity.setParticipantDecimal(budgetData.getParticipantDecimal());
		updateBudgetEntity.setSamCertificateInd(budgetData.getSamCertificateInd());
		updateBudgetEntity.setPersonnelNonFedAmt(budgetData.getPersonnelNonFedAmt());
		updateBudgetEntity.setPersonnelReqFedAmt(budgetData.getPersonnelReqFedAmt());
		updateBudgetEntity.setFringeReqFedAmt(budgetData.getFringeReqFedAmt());
		updateBudgetEntity.setFringeRecFedAmt(budgetData.getFringeRecFedAmt());
		updateBudgetEntity.setFringeNonFedAmt(budgetData.getFringeNonFedAmt());
		updateBudgetEntity.setTravelNonFedAmt(budgetData.getTravelNonFedAmt());
		updateBudgetEntity.setTravelRecFedAmt(budgetData.getTravelRecFedAmt());
		updateBudgetEntity.setTravelReqFedAmt(budgetData.getTravelReqFedAmt());
		updateBudgetEntity.setEquipNonFedAmt(budgetData.getEquipNonFedAmt());
		updateBudgetEntity.setEquipRecFedAmt(budgetData.getEquipRecFedAmt());
		updateBudgetEntity.setEquipReqFedAmt(budgetData.getEquipReqFedAmt());
		updateBudgetEntity.setSupplyNonFedAmt(budgetData.getSupplyNonFedAmt());
		updateBudgetEntity.setSupplyRecFedAmt(budgetData.getSupplyRecFedAmt());
		updateBudgetEntity.setSupplyReqFedAmt(budgetData.getSupplyReqFedAmt());
		updateBudgetEntity.setContractNonFedAmt(budgetData.getContractNonFedAmt());
		updateBudgetEntity.setContractRecFedAmt(budgetData.getContractRecFedAmt());
		updateBudgetEntity.setContractReqFedAmt(budgetData.getContractReqFedAmt());
		updateBudgetEntity.setConstructNonFedAmt(budgetData.getConstructNonFedAmt());
		updateBudgetEntity.setConstructRecFedAmt(budgetData.getConstructRecFedAmt());
		updateBudgetEntity.setConstructReqFedAmt(budgetData.getConstructReqFedAmt());
		updateBudgetEntity.setOtherNonFedAmt(budgetData.getOtherNonFedAmt());
		updateBudgetEntity.setOtherRecFedAmt(budgetData.getOtherRecFedAmt());
		updateBudgetEntity.setOtherReqFedAmt(budgetData.getOtherReqFedAmt());
		updateBudgetEntity.setTrainingNonFedAmt(budgetData.getTrainingNonFedAmt());
		updateBudgetEntity.setTrainingRecFedAmt(budgetData.getTrainingRecFedAmt());
		updateBudgetEntity.setTrainingReqFedAmt(budgetData.getTrainingReqFedAmt());
		updateBudgetEntity.setIndirectNonFedAmt(budgetData.getIndirectNonFedAmt());
		updateBudgetEntity.setIndirectRecFedAmt(budgetData.getIndirectRecFedAmt());
		updateBudgetEntity.setIndirectReqFedAmt(budgetData.getIndirectReqFedAmt());

		BudgetInfomationEntity updatedBudgetEnt = modifyRepository.save(updateBudgetEntity);

		return updatedBudgetEnt;
	}

	/**
	 * Update Budget information using BudgetPerdId
	 * 
	 * @param budgetPerdId
	 * @param budgetData
	 * @return
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 */
	@Transactional
	public BudgetInfomationEntity updateBudgetUsingId(BudgetInfomationEntity budgetData) {
		return modifyRepository.save(budgetData);
	}
}
