package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.BudgetInfomationEntity;
import com.example.service.ModifyBudgetService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("demo")
public class ModifyBudgetController {

	@Autowired
	protected ModifyBudgetService budgetService;

	/**
	 * Return all budgets based on application-id.
	 * 
	 * @return
	 */
	@ApiOperation(value = "Get all budgets based on application-id")
	@GetMapping("/return-all-budgets/{applicationId}")
	public @ResponseBody Iterable<BudgetInfomationEntity> returnAllBudgets(
			@PathVariable(value = "applicationId") int id) {

		return budgetService.getAllBudgets(id);
	}

	/**
	 * Remove a budget.
	 * 
	 * @return
	 */
	@ApiOperation(value = "Delete a budget based on an budget_perd_id.")
	@DeleteMapping("/remove-budget/{budgetPerdId}")
	public @ResponseBody String removeBudget(@PathVariable(value = "budgetPerdId") int id) {

		return budgetService.deleteSingleBudget(id);
	}

	/**
	 * Add a new budget entry.
	 * 
	 * @param budgetData
	 * @return
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 */
	@ApiOperation(value = "Add a new budget entry.")
	@PostMapping("/add-budget")
	public @ResponseBody BudgetInfomationEntity addBudget(@RequestBody BudgetInfomationEntity budgetData)
			throws JsonMappingException, JsonProcessingException {

		return budgetService.createBudget(budgetData);
	}

	/*	*//**
			 * Update an existing budget entry.
			 * 
			 * @param applicationId
			 * @param budgetPerdSeq
			 * @param budgetData
			 * @return
			 *//*
				 * @ApiOperation(value = "Update a budget entry.")
				 * 
				 * @PutMapping("/update-budget/{applicationId}/{budgetPerdSeq}")
				 * public @ResponseBody BudgetInfomationEntity
				 * updateSingleBudget(@PathVariable(value="applicationId") int applicationId,
				 * 
				 * @PathVariable(value="budgetPerdSeq") int budgetPerdSeq, @RequestBody
				 * BudgetInfomationEntity budgetData) { return
				 * budgetService.updateBudget(applicationId, budgetPerdSeq, budgetData); }
				 */

	/**
	 * Update an existing budget entry.
	 * 
	 * @param applicationId
	 * @param budgetPerdSeq
	 * @param budgetData
	 * @return
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 */
	@ApiOperation(value = "Update a budget entry.")
	@PutMapping("/update-single-budget")
	public @ResponseBody BudgetInfomationEntity updateBudget(@RequestBody BudgetInfomationEntity budgetData) {
		return budgetService.updateBudgetUsingId(budgetData);
	}

}
