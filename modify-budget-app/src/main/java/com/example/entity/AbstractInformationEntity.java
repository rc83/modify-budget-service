package com.example.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="abstract_info")
public class AbstractInformationEntity {
	
	public AbstractInformationEntity() {
		// TODO Auto-generated constructor stub
	}
	
	@Id
	private Integer abstractId;
	private String abstractFileName;
	private Date dateSelected;
	private Date lastModifiedOn;
	private String modifiedUser;
	private String comments;
	private Integer applicationId;

	public Integer getAbstractId() {
		return abstractId;
	}
	public void setAbstractID(int abstractId) {
		this.abstractId = abstractId;
	}
	public String getAbstractFileName() {
		return abstractFileName;
	}
	public void setAbstractFileName(String abstractFileName) {
		this.abstractFileName = abstractFileName;
	}
	public Date getDateSelected() {
		return dateSelected;
	}
	public void setDateSelected(Date dateSelected) {
		this.dateSelected = dateSelected;
	}
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	public String getModifiedUser() {
		return modifiedUser;
	}
	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((abstractFileName == null) ? 0 : abstractFileName.hashCode());
		result = prime * result + abstractId;
		result = prime * result + applicationId;
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((dateSelected == null) ? 0 : dateSelected.hashCode());
		result = prime * result + ((lastModifiedOn == null) ? 0 : lastModifiedOn.hashCode());
		result = prime * result + ((modifiedUser == null) ? 0 : modifiedUser.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractInformationEntity other = (AbstractInformationEntity) obj;
		if (abstractFileName == null) {
			if (other.abstractFileName != null)
				return false;
		} else if (!abstractFileName.equals(other.abstractFileName))
			return false;
		if (abstractId != other.abstractId)
			return false;
		if (applicationId != other.applicationId)
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (dateSelected == null) {
			if (other.dateSelected != null)
				return false;
		} else if (!dateSelected.equals(other.dateSelected))
			return false;
		if (lastModifiedOn == null) {
			if (other.lastModifiedOn != null)
				return false;
		} else if (!lastModifiedOn.equals(other.lastModifiedOn))
			return false;
		if (modifiedUser == null) {
			if (other.modifiedUser != null)
				return false;
		} else if (!modifiedUser.equals(other.modifiedUser))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AbstractInformation [abstractId=" + abstractId + ", abstractFileName=" + abstractFileName
				+ ", dateSelected=" + dateSelected + ", lastModifiedOn=" + lastModifiedOn + ", modifiedUser="
				+ modifiedUser + ", comments=" + comments + ", applicationId=" + applicationId + "]";
	}
	

}
