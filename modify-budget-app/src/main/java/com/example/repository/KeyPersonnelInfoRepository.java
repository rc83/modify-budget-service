package com.example.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.KeyPersonnelInfoEntity;

public interface KeyPersonnelInfoRepository extends CrudRepository<KeyPersonnelInfoEntity, Integer> {
	
}
