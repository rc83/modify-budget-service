package com.example.exception;

public class IncorrectBudgetApplicationMappingException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IncorrectBudgetApplicationMappingException(String s){  
		  super(s);  
	}  
}
