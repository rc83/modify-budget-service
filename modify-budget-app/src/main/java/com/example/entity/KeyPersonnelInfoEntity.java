package com.example.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="key_personnel_info")
public class KeyPersonnelInfoEntity {

	public KeyPersonnelInfoEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer keyPersonnelId;
	//private Integer budgetPerdId;
	//@NotNull
	private String keyPersonnelTitleCd;
	private String firstName;
	private String lastName;
	private String middleInitial;
	private Float levelOfEffort;
	private String phoneNumber;
	private String email;
	
    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "budget_perd_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private BudgetInfomationEntity budgetEntity;
    
    public BudgetInfomationEntity getBudgetEntity() {
		return budgetEntity;
	}
	public void setBudgetEntity(BudgetInfomationEntity budgetEntity) {
		this.budgetEntity = budgetEntity;
	}
	public Integer getKeyPersonnelId() {
		return keyPersonnelId;
	}
	public void setKeyPersonnelId(Integer keyPersonnelId) {
		this.keyPersonnelId = keyPersonnelId;
	}

	public String getKeyPersonnelTitleCd() {
		return keyPersonnelTitleCd;
	}
	public void setKeyPersonnelTitleCd(String keyPersonnelTitleCd) {
		this.keyPersonnelTitleCd = keyPersonnelTitleCd;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleInitial() {
		return middleInitial;
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
	public Float getLevelOfEffort() {
		return levelOfEffort;
	}
	public void setLevelOfEffort(Float levelOfEffort) {
		this.levelOfEffort = levelOfEffort;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
}
