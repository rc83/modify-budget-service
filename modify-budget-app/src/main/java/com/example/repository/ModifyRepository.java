package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.entity.BudgetInfomationEntity;

public interface ModifyRepository extends CrudRepository<BudgetInfomationEntity, Integer> {
	
	@Query(value="select * from budget_info u where u.application_id=?1" , nativeQuery=true)
	List<BudgetInfomationEntity> getBudgetsByAppId(int id);
	
	@Query(value="select * from budget_info u where u.application_id=?1 and u.budget_perd_seq=?2" , nativeQuery=true)
	BudgetInfomationEntity getBudgetByAppIdAndBudgetSeq(int a, int b);
	
	@Query(value="select * from budget_info u where u.budget_perd_id=?1" , nativeQuery=true)
	BudgetInfomationEntity getBudgetByBudgetPerdId(int a);

}
